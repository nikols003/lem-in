/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem-in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 13:32:36 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 13:33:09 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H
# include "libft/libft.h"
# include "libft/ft_printf/libftprintf.h"
# include <stdlib.h>
# include <fcntl.h>

typedef struct		s_flat
{
	int				x;
	int				y;
	int				*link;
	int				count_l;
	int				ant;
	int				race;
	char			*name;
}					t_flat;

typedef struct		s_a
{
	int				count_ant;
	int				count_flat;
	char			*start;
	char			*end;
	t_flat			*flat;
	int				*ant_tunel;
	int				count_tunel;
}					t_a;

void				ft_parse_all(t_a *s);
void				ft_count_flat(t_a *s);
int					ft_nb(t_a *s, char *name);
void				ft_algoritm(t_a *s, char **str);
char				*ft_obrez(char *line);
void				ft_save_flat(char *line, t_a *s);
void				ft_save_link(char *line, t_a *s);
void				ft_save_link2(char *line, t_a *s);
int					ft_write_flat(char *str, int flat, t_a *s);
int					ft_algo(t_a *s, int count, int flat, int prev_flat);
int					ft_find_nb(char *str, int flat);
int					ft_len(char *str);
void				ft_show_step(t_a *s, char *str);
int					ft_next_step(t_a *s, char *str);
void				ft_run_run(t_a *s, char *str);
void				ft_print_end(t_a *s, int z);
void				ft_show_step_multi(t_a *s, char **str, int len);
int					ft_count_step(t_a *s, char **str, int *array);
int					ft_sum_tunel(t_a *s);
void				ft_run_ant(t_a *s, char **str);
void				ft_formula(t_a *s, char **str);
int					ft_find_small(t_a *s, int tunel, char **str, int min);
int					ft_recurs(t_a *s, int tunel, char **str);
void				ft_run_two(t_a *s, char **str, int len);

#endif
