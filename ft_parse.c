/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 13:31:58 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 13:47:58 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	ft_formula(t_a *s, char **str)
{
	int i;
	int tmp;

	i = -1;
	s->ant_tunel = (int *)malloc(sizeof(int) * s->count_tunel + 1);
	while (++i < s->count_tunel)
		s->ant_tunel[i] = 0;
	if (s->count_tunel > 1)
	{
		tmp = ft_recurs(s, 0, str);
		ft_find_small(s, 0, str, tmp);
	}
}

void	ft_run_ant(t_a *s, char **str)
{
	int len;

	len = 0;
	while (str[len])
		len++;
	if (len == 1 || s->count_ant < 2)
		ft_run_run(s, str[0]);
	else
	{
		s->count_tunel = len;
		ft_formula(s, str);
		ft_run_two(s, str, len);
	}
}

void	ft_parse_help(t_a *s, char *line, int fd)
{
	char *p;

	if (ft_strcmp(line, "##start") == 0)
	{
		get_next_line(fd, &line);
		ft_printf("%s\n", line);
		s->start = ft_obrez(line);
	}
	if (ft_strcmp(line, "##end") == 0)
	{
		get_next_line(fd, &line);
		ft_printf("%s\n", line);
		s->end = ft_obrez(line);
	}
	if ((p = ft_strchr(line, 32)) && line[0] != '#')
		if (ft_strchr(p, 32))
			ft_save_flat(line, s);
}

void	ft_count_ant(t_a *s, int fd)
{
	char	*line;
	int		i;

	i = 0;
	while (get_next_line(fd, &line) > 0)
	{
		if (line[0] == '#')
			ft_printf("%s\n", line);
		while (ft_isdigit(line[i]))
			i++;
		if (i == ft_strlen(line))
		{
			s->count_ant = (int)ft_atoi(line);
			return ;
		}
	}
	exit(ft_printf("No ants\n"));
}

void	ft_parse_all(t_a *s)
{
	int		fd;
	char	*line;

	fd = 0;
	ft_count_ant(s, 0);
	ft_printf("%d\n", s->count_ant);
	while (get_next_line(fd, &line) > 0)
	{
		ft_printf("%s\n", line);
		ft_parse_help(s, line, fd);
		if (ft_strchr(line, 45))
		{
			ft_save_link(line, s);
			ft_save_link2(line, s);
		}
	}
	ft_printf(" \n");
}
