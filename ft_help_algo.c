/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_help_algo.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 13:31:00 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 13:31:06 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	ft_help_algo(t_a *s, int count, int flat, int prev_flat)
{
	int i;

	i = 0;
	while (i < s->flat[flat].count_l)
	{
		if (s->flat[s->flat[flat].link[i]].ant == 0 && \
		s->flat[flat].link[i] != prev_flat)
		{
			if (s->flat[s->flat[flat].link[i]].race == 999)
				s->flat[s->flat[flat].link[i]].race = count + 1;
			if (s->flat[s->flat[flat].link[i]].race > count + 1)
				s->flat[s->flat[flat].link[i]].race = count + 1;
		}
		i++;
	}
}

int		ft_algo(t_a *s, int count, int flat, int prev_flat)
{
	int i;

	ft_help_algo(s, count, flat, prev_flat);
	if (flat == ft_nb(s, s->end))
	{
		s->flat[flat].race = count;
		return (1);
	}
	s->flat[flat].ant = 1;
	i = 0;
	while (i < s->flat[flat].count_l)
	{
		if (s->flat[s->flat[flat].link[i]].ant == 0 && \
		s->flat[flat].link[i] != prev_flat)
			ft_algo(s, count + 1, s->flat[flat].link[i], flat);
		i++;
	}
	s->flat[flat].ant = 0;
	return (1);
}

int		ft_write_flat(char *str, int flat, t_a *s)
{
	int		i;
	int		j;
	char	*tmp;

	if (flat == ft_nb(s, s->start))
		return (0);
	i = ft_strlen(str);
	tmp = ft_itoa(flat);
	str[i++] = '-';
	j = 0;
	while (tmp[j])
	{
		str[i++] = tmp[j++];
	}
	str[i] = '\0';
	return (j + 1);
}
