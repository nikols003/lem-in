/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visual.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 12:05:09 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 12:05:41 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VISUAL_H
# define VISUAL_H

# include "../libft/libft.h"
# include "../libft/ft_printf/libftprintf.h"
# include <stdlib.h>
# include <fcntl.h>
# include "mlx.h"
# include "math.h"

typedef struct		s_point
{
	int		x;
	int		y;
	int		z;
	int		color1;
	int		color2;
	int		color3;
	int		newline;
}					t_point;

typedef struct		s_fifi
{
	void	*mlx;
	void	*win;
	t_point	*a;
	int		count;
	int		width;
	int		height;
	double	scale_x;
	double	scale_y;
	double	alfa;
	double	beta;
	int		left;
	int		top;
}					t_fifi;

typedef struct		s_flat
{
	int				x;
	int				y;
	int				*link;
	int				count_l;
	int				ant;
	int				race;
	char			*name;
}					t_flat;

typedef struct		s_a
{
	int				count_ant;
	int				count_flat;
	char			*start;
	char			*end;
	t_flat			*flat;
	t_fifi			*fifi;
	int				*ant_tunel;
	int				count_tunel;
}					t_a;

void				ft_start(t_a *s);
void				ft_print_line(t_point k1, t_point k2, void *mlx, void *win);
int					ft_color_x(int x, t_point k1, t_point k2);
int					ft_color_y(int y, t_point k1, t_point k2);
char				*ft_obrez(char *line);
int					ft_nb(t_a *s, char *name);
void				ft_write_tunel(t_a *s);
void				ft_write_flat(t_a *s);
void				ft_count_ant(t_a *s);

#endif
