/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/20 14:10:39 by mdubina           #+#    #+#             */
/*   Updated: 2017/01/20 14:20:52 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"

void	ft_print_line(t_point k1, t_point k2, void *mlx, void *win)
{
	t_point	tmp;
	double	x;
	double	y;

	if (k1.x + k1.y > k2.x + k2.y)
	{
		tmp = k1;
		k1 = k2;
		k2 = tmp;
	}
	x = k1.x;
	while (x < k2.x)
	{
		y = k1.y + (x - k1.x) * (k2.y - k1.y) / (k2.x - k1.x);
		mlx_pixel_put(mlx, win, x, y, ft_color_x(x, k1, k2));
		x++;
	}
	y = k1.y;
	while (y < k2.y)
	{
		x = k1.x + (y - k1.y) * (k2.x - k1.x) / (k2.y - k1.y);
		mlx_pixel_put(mlx, win, x, y, ft_color_y(y, k1, k2));
		y++;
	}
}

int		ft_color_x(int x, t_point k1, t_point k2)
{
	int color;
	int color1;
	int color2;
	int color3;

	if (k1.color1 == k2.color1 && k1.color2 == k2.color2 && \
		k1.color3 == k2.color3)
	{
		color = (k1.color1 << 16) | (k1.color2 << 8) | k1.color3;
		return (color);
	}
	else
	{
		color1 = k1.color1 + (k2.color1 - k1.color1) / \
			(k2.x - k1.x) * (x - k1.x + 1);
		color2 = k1.color2 + (k2.color2 - k1.color2) / \
			(k2.x - k1.x) * (x - k1.x + 1);
		color3 = k1.color3 + (k2.color3 - k1.color3) / \
			(k2.x - k1.x) * (x - k1.x + 1);
		color = ((color1 << 16) | (color2 << 8) | color3);
		return (color);
	}
}

int		ft_color_y(int y, t_point k1, t_point k2)
{
	int color;
	int color1;
	int color2;
	int color3;

	if (k1.color1 == k2.color1 && k1.color2 == k2.color2 && \
		k1.color3 == k2.color3)
	{
		color = (k1.color1 << 16) | (k1.color2 << 8) | k1.color3;
		return (color);
	}
	else
	{
		color1 = k1.color1 + (k2.color1 - k1.color1) / \
			(k2.y - k1.y) * (y - k1.y + 1);
		color2 = k1.color2 + (k2.color2 - k1.color2) / \
			(k2.y - k1.y) * (y - k1.y + 1);
		color3 = k1.color3 + (k2.color3 - k1.color3) / \
			(k2.y - k1.y) * (y - k1.y + 1);
		color = ((color1 << 16) | (color2 << 8) | color3);
		return (color);
	}
}
