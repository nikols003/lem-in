/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tunel.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 12:04:25 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 13:50:44 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"

void	ft_put_flat(t_a *s, int x, int y, int flat)
{
	int i;
	int j;

	i = x + 80;
	while (i != x)
	{
		j = y + 50;
		while (j != y)
		{
			mlx_pixel_put(s->fifi->mlx, s->fifi->win, i, j, 0x0CD57E);
			j--;
		}
		i--;
	}
	mlx_string_put(s->fifi->mlx, s->fifi->win, i, j - 20, 0xC7E7EB, \
	s->flat[flat].name);
}

void	ft_write_flat(t_a *s)
{
	int i;
	int x;
	int y;

	i = 0;
	while (i < s->count_flat - 1)
	{
		x = s->flat[i].x * 10 - 10;
		y = s->flat[i].y * 10 - 10;
		ft_put_flat(s, x, y, i);
		i++;
	}
}

void	ft_help_tunel(t_a *s, int i, t_point a)
{
	t_point	b;
	int		j;
	int		z;

	b.color1 = 153;
	b.color2 = 102;
	b.color3 = 0;
	j = 0;
	while (j < s->flat[i].count_l)
	{
		z = 0;
		while (z < 30)
		{
			b.x = s->flat[s->flat[i].link[j]].x * 10 - z;
			b.y = s->flat[s->flat[i].link[j]].y * 10 - z;
			ft_print_line(a, b, s->fifi->mlx, s->fifi->win);
			z++;
		}
		j++;
	}
}

void	ft_write_tunel(t_a *s)
{
	int		i;
	t_point	a;

	i = 0;
	a.color1 = 153;
	a.color2 = 102;
	a.color3 = 0;
	while (i < s->count_flat)
	{
		a.x = s->flat[i].x * 10;
		a.y = s->flat[i].y * 10;
		ft_help_tunel(s, i, a);
		i++;
	}
}

void	ft_count_ant(t_a *s)
{
	char	*line;
	int		i;

	i = 0;
	while (get_next_line(0, &line) > 0)
	{
		while (ft_isdigit(line[i]))
			i++;
		if (i == ft_strlen(line))
		{
			s->count_ant = (int)ft_atoi(line);
			return ;
		}
	}
	exit(ft_printf("No ants\n"));
}
