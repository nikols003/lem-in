/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visual.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 12:04:35 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 12:04:41 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"

void	ft_save_flat(char *line, t_a *s)
{
	int i;

	s->flat[s->count_flat].name = ft_obrez(line);
	i = 0;
	while (line[i] != ' ')
		i++;
	i++;
	s->flat[s->count_flat].x = (int)ft_atoi(&line[i]);
	while (ft_isdigit(line[i]))
		i++;
	i++;
	s->flat[s->count_flat].y = (int)ft_atoi(&line[i]);
	s->flat[s->count_flat].count_l = 0;
	s->flat[s->count_flat].link = (int *)malloc(sizeof(int) * 100);
	s->flat[s->count_flat].ant = 0;
	s->flat[s->count_flat].race = 999;
	s->count_flat++;
}

void	ft_save_link(char *line, t_a *s)
{
	int nb;

	nb = ft_nb(s, ft_obrez(line));
	line = ft_strchr(line, 45);
	line++;
	s->flat[nb].link[s->flat[nb].count_l] = ft_nb(s, ft_obrez(line));
	s->flat[nb].count_l++;
}

void	ft_save_link2(char *line, t_a *s)
{
	int nb;
	int i;

	i = 0;
	while (line[i] != '-')
		i++;
	i++;
	nb = ft_nb(s, &line[i]);
	s->flat[nb].link[s->flat[nb].count_l] = ft_nb(s, ft_obrez(line));
	s->flat[nb].count_l++;
}

void	help_main(t_a *s, char *line)
{
	char	*p;

	if (ft_strcmp(line, "##end") == 0)
	{
		get_next_line(0, &line);
		s->end = ft_obrez(line);
	}
	if ((p = ft_strchr(line, 32)) && line[0] != '#')
		if (ft_strchr(p, 32))
			ft_save_flat(line, s);
	if (ft_strchr(line, 45))
	{
		ft_save_link(line, s);
		ft_save_link2(line, s);
	}
	if (line[0] == ' ')
		ft_start(s);
}

int		main(void)
{
	char	*line;
	t_a		s[1];

	s->flat = (t_flat *)malloc(sizeof(t_flat) * 100);
	ft_count_ant(s);
	s->count_flat = 0;
	while (get_next_line(0, &line) > 0)
	{
		if (ft_strcmp(line, "##start") == 0)
		{
			get_next_line(0, &line);
			s->start = ft_obrez(line);
		}
		help_main(s, line);
	}
	return (1);
}
