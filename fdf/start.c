/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/20 14:16:34 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 12:04:16 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"

int		help_write(t_a *s, char *line)
{
	int i;
	int ant;
	int z;

	i = 0;
	z = 0;
	while (line[i])
	{
		ant = (int)ft_atoi(&line[i++ + 1]);
		while (ft_isdigit(line[i]))
			i++;
		i++;
		mlx_string_put(s->fifi->mlx, s->fifi->win, s->flat[ft_nb(s, \
		ft_obrez(&line[i]))].x * 10, s->flat[ft_nb(s, ft_obrez(&line[i]))].y \
		* 10, 0x00FF, ft_strjoin("ant #", ft_itoa(ant)));
		while (line[i] != ' ' && line[i])
			i++;
		if (line[i])
			i++;
		if (ant > z)
			z = ant;
	}
	return (z);
}

int		ft_key_function(int key, t_a *s)
{
	char	*line;
	int		z;

	mlx_clear_window(s->fifi->mlx, s->fifi->win);
	if (key == 124)
	{
		ft_write_tunel(s);
		ft_write_flat(s);
		if (!get_next_line(0, &line))
			exit(1);
		z = help_write(s, line);
		if (s->count_ant > 0)
			mlx_string_put(s->fifi->mlx, s->fifi->win, s->flat[ft_nb(s, \
			ft_obrez(s->start))].x * 10 - 10, \
			s->flat[ft_nb(s, ft_obrez(s->start))].y * 10, 0x00FF, \
			ft_strjoin("Count-", ft_itoa(s->count_ant - z)));
	}
	if (key == 53)
		exit(1);
	return (0);
}

void	ft_start(t_a *s)
{
	t_fifi fifi[1];

	s->fifi = fifi;
	s->fifi->mlx = mlx_init();
	s->fifi->win = mlx_new_window(s->fifi->mlx, 1200, 1200, "Hello");
	ft_write_tunel(s);
	ft_write_flat(s);
	mlx_key_hook(s->fifi->win, ft_key_function, s);
	mlx_loop(s->fifi->mlx);
	return ;
}

char	*ft_obrez(char *line)
{
	int		i;
	char	*tmp;

	i = 0;
	tmp = (char *)malloc(sizeof(char) * ft_strlen(line));
	while (line[i] != ' ' && line[i] != '-' && line[i])
	{
		tmp[i] = line[i];
		i++;
	}
	tmp[i] = '\0';
	return (tmp);
}

int		ft_nb(t_a *s, char *name)
{
	int i;

	i = 0;
	while (i < s->count_flat)
	{
		if (strcmp(name, s->flat[i].name) == 0)
			return (i);
		i++;
	}
	ft_printf("EMPTY\n");
	exit(11);
}
