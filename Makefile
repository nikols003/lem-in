NAME = lem-in

GCC = gcc -Wall -Wextra -Werror

FRAMEWORKS = -framework OpenGL -framework AppKit

SRC =	main.c \
		ft_parse.c \
		ft_algoritm.c \
		ft_parse_help.c \
		ft_help_algo.c \
		ft_algoritm_help.c \
		ft_algoritm_help2.c

OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
		gcc -Wall -Wextra -Werror -c $(SRC)
		cd libft/ && $(MAKE)
		cd libft/ft_printf/ && $(MAKE)
		gcc -o $(NAME) $(OBJ) libft/ft_printf/libftprintf.a libft/libft.a

%.o : %.c
	$(GCC) -c -o  $@ $<

clean:
		rm -f $(OBJ)
		make clean -C libft/
		make clean -C libft/ft_printf/

fclean: clean
		make fclean -C libft/
		make fclean -C libft/ft_printf/
		rm -f $(NAME)

re: fclean all
