/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_help.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 13:32:05 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 13:32:07 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

char	*ft_obrez(char *line)
{
	int		i;
	char	*tmp;

	i = 0;
	tmp = (char *)malloc(sizeof(char) * ft_strlen(line));
	while (line[i] != ' ' && line[i] != '-' && line[i])
	{
		tmp[i] = line[i];
		i++;
	}
	tmp[i] = '\0';
	return (tmp);
}

int		ft_nb(t_a *s, char *name)
{
	int i;

	i = 0;
	while (i < s->count_flat)
	{
		if (strcmp(name, s->flat[i].name) == 0)
			return (i);
		i++;
	}
	ft_printf("No room with name {%s}\n", name);
	exit(11);
}

void	ft_save_flat(char *line, t_a *s)
{
	int i;

	s->flat[s->count_flat].name = ft_obrez(line);
	i = 0;
	while (line[i] != ' ')
		i++;
	i++;
	s->flat[s->count_flat].x = (int)ft_atoi(&line[i]);
	while (ft_isdigit(line[i]))
		i++;
	i++;
	s->flat[s->count_flat].y = (int)ft_atoi(&line[i]);
	s->flat[s->count_flat].count_l = 0;
	s->flat[s->count_flat].link = (int *)malloc(sizeof(int) * 100);
	s->flat[s->count_flat].ant = 0;
	s->flat[s->count_flat].race = 999;
	s->count_flat++;
}

void	ft_save_link(char *line, t_a *s)
{
	int nb;

	nb = ft_nb(s, ft_obrez(line));
	line = ft_strchr(line, 45);
	line++;
	s->flat[nb].link[s->flat[nb].count_l] = ft_nb(s, ft_obrez(line));
	s->flat[nb].count_l++;
}

void	ft_save_link2(char *line, t_a *s)
{
	int nb;
	int i;

	i = 0;
	while (line[i] != '-')
		i++;
	i++;
	nb = ft_nb(s, &line[i]);
	s->flat[nb].link[s->flat[nb].count_l] = ft_nb(s, ft_obrez(line));
	s->flat[nb].count_l++;
}
