/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algoritm_help.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 13:31:29 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 13:31:31 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		ft_find_nb(char *str, int flat)
{
	int i;
	int count;

	i = 0;
	count = 0;
	while (str[i])
	{
		if (str[i] == '-')
			count++;
		if (count == flat)
			return ((int)ft_atoi(&str[i + 1]));
		i++;
	}
	return (0);
}

int		ft_len(char *str)
{
	int i;
	int count;

	i = 0;
	count = 0;
	while (str[i])
	{
		if (str[i] == '-')
			count++;
		i++;
	}
	return (count);
}

int		ft_next_step(t_a *s, char *str)
{
	int len;
	int count;

	len = ft_len(str) - 1;
	count = 0;
	while (len)
	{
		if (s->flat[ft_find_nb(str, len)].ant)
			count++;
		if (s->flat[ft_find_nb(str, len)].ant)
		{
			s->flat[ft_find_nb(str, len)].ant = 0;
			s->flat[ft_find_nb(str, len + 1)].ant++;
		}
		len--;
	}
	if (!count)
		return (0);
	return (1);
}

void	ft_show_step(t_a *s, char *str)
{
	int len;
	int count;

	len = ft_len(str);
	if (s->flat[ft_nb(s, s->end)].ant)
		count = s->flat[ft_nb(s, s->end)].ant;
	else
		count = 1;
	while (len)
	{
		if (s->flat[ft_find_nb(str, len)].ant)
			ft_printf("L%d-%s ", count++, s->flat[ft_find_nb(str, len)].name);
		len--;
	}
	ft_printf("\n");
}

void	ft_run_run(t_a *s, char *str)
{
	int ant;
	int z;

	z = 0;
	ant = s->count_ant;
	while (s->flat[ft_nb(s, s->end)].ant != ant)
	{
		if (s->count_ant)
		{
			s->flat[ft_find_nb(str, 1)].ant++;
			s->count_ant--;
		}
		ft_show_step(s, str);
		z = ft_next_step(s, str);
	}
	if (z)
		ft_show_step(s, str);
}
