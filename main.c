/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 13:31:43 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 13:31:46 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		ft_best_way(char *str, t_a *s, int flat, int deep)
{
	int i;
	int z;

	if (deep <= s->flat[ft_nb(s, s->end)].race && flat == ft_nb(s, s->end))
	{
		ft_write_flat(str, flat, s);
		return (1);
	}
	i = -1;
	s->flat[flat].ant = 1;
	while (++i < s->flat[flat].count_l)
	{
		if (s->flat[s->flat[flat].link[i]].ant == 0)
		{
			z = ft_write_flat(str, flat, s);
			if (ft_best_way(str, s, s->flat[flat].link[i], deep + 1) == 1)
				return (1);
			str[ft_strlen(str) - z] = '\0';
		}
	}
	s->flat[flat].ant = 0;
	return (0);
}

char	*ft_count_end(t_a *s, char *str, int z)
{
	if (ft_best_way(str, s, ft_nb(s, s->start), 0))
		return (str);
	else if (z == 0)
		exit(ft_printf("No possible solution\n"));
	return (NULL);
}

void	ft_search_way_help(char **str, t_a *s)
{
	int z;
	int i;

	z = 0;
	while (z < 999)
	{
		s->flat[ft_nb(s, s->start)].race = 0;
		s->flat[ft_nb(s, s->start)].ant = 0;
		ft_algo(s, 0, ft_nb(s, s->start), 555);
		str[z][0] = ft_nb(s, s->start) + 48;
		str[z][1] = '\0';
		str[z] = ft_count_end(s, str[z], z);
		if (str[z] == NULL || s->flat[ft_nb(s, s->end)].race == 1)
		{
			str[z + 1] = NULL;
			break ;
		}
		i = 0;
		while (i < s->count_flat)
			s->flat[i++].race = 999;
		z++;
	}
}

char	**ft_search_way(t_a *s)
{
	int		i;
	char	**str;

	str = (char **)malloc(sizeof(char *) * 999);
	i = 0;
	while (i < 998)
	{
		str[i] = (char *)malloc(sizeof(char) * 9999);
		str[i][0] = '\0';
		i++;
	}
	str[998] = 0;
	ft_search_way_help(str, s);
	return (str);
}

int		main(void)
{
	t_a		s[1];
	char	**str;

	s->flat = (t_flat *)malloc(sizeof(t_flat) * 100);
	s->count_flat = 0;
	ft_parse_all(s);
	if (!s->end || !s->start)
		return (ft_printf("No mandatory comments\n"));
	str = ft_search_way(s);
	ft_algoritm(s, str);
	return (0);
}
