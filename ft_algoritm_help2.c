/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algoritm_help2.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 13:31:35 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 13:31:37 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	ft_print_end(t_a *s, int z)
{
	while (z)
	{
		ft_printf("L%d-%s ", s->flat[ft_nb(s, s->end)].ant--, \
		s->flat[ft_nb(s, s->end)].name);
		z--;
	}
	ft_printf("\n");
}

void	ft_help_show_step(t_a *s, int *count)
{
	if (s->flat[ft_nb(s, s->end)].ant)
		*count = s->flat[ft_nb(s, s->end)].ant + 1;
	else
		*count = 1;
}

void	ft_show_step_multi(t_a *s, char **str, int len)
{
	int c;
	int i;
	int si;
	int z;

	ft_help_show_step(s, &c);
	si = ft_len(str[0]) + 1;
	while (--si)
	{
		i = 0;
		z = s->flat[ft_find_nb(str[i], si)].ant;
		while (i < len)
		{
			if (ft_find_nb(str[i], si) == ft_nb(s, s->end) && \
			s->flat[ft_find_nb(str[i], si)].ant)
				ft_printf("L%d-%s ", z--, s->flat[ft_find_nb(str[i], si)].name);
			else if (s->flat[ft_find_nb(str[i], si)].ant)
				ft_printf("L%d-%s ", c++, s->flat[ft_find_nb(str[i], si)].name);
			i++;
		}
	}
	ft_printf("\n");
}

int		ft_count_step(t_a *s, char **str, int *array)
{
	int i;
	int res;

	i = 0;
	res = 0;
	while (i < s->count_tunel)
	{
		if (array[i] && res < ft_len(str[i]) + array[i] - 1)
			res = ft_len(str[i]) + array[i] - 1;
		i++;
	}
	return (res);
}

int		ft_sum_tunel(t_a *s)
{
	int i;
	int res;

	i = 0;
	res = 0;
	while (i < s->count_tunel)
	{
		res += s->ant_tunel[i];
		i++;
	}
	return (res);
}
