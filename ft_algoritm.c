/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algoritm.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 13:31:17 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/19 13:31:22 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	ft_run_two(t_a *s, char **str, int len)
{
	int ant;
	int z;
	int i;

	ant = s->count_ant;
	while (s->flat[ft_nb(s, s->end)].ant != ant)
	{
		i = -1;
		while (++i < len)
		{
			if (s->count_ant && s->ant_tunel[i])
			{
				s->flat[ft_find_nb(str[i], 1)].ant++;
				s->count_ant--;
				s->ant_tunel[i]--;
			}
		}
		ft_show_step_multi(s, str, len);
		z = 0;
		i = -1;
		while (++i < len)
			z += ft_next_step(s, str[i]);
	}
	ft_print_end(s, z);
}

int		ft_recurs(t_a *s, int tunel, char **str)
{
	int res;
	int tmp;

	res = 999;
	tmp = 999;
	s->ant_tunel[tunel] = 0;
	while (s->ant_tunel[tunel] <= s->count_ant)
	{
		if (tunel + 1 < s->count_tunel)
			tmp = ft_recurs(s, tunel + 1, str);
		if (res > ft_count_step(s, str, s->ant_tunel) && \
		ft_sum_tunel(s) == s->count_ant)
			res = ft_count_step(s, str, s->ant_tunel);
		if (tmp < res)
			res = tmp;
		s->ant_tunel[tunel]++;
	}
	if (tmp < res)
		return (tmp);
	return (res);
}

int		ft_find_small(t_a *s, int tunel, char **str, int min)
{
	int res;

	res = 999;
	s->ant_tunel[tunel] = 0;
	while (s->ant_tunel[tunel] <= s->count_ant)
	{
		if (tunel + 1 < s->count_tunel)
		{
			if (!ft_find_small(s, tunel + 1, str, min))
				return (0);
		}
		if (res > ft_count_step(s, str, s->ant_tunel) && \
		ft_sum_tunel(s) == s->count_ant)
			res = ft_count_step(s, str, s->ant_tunel);
		if (min == res)
			return (0);
		s->ant_tunel[tunel]++;
	}
	return (1);
}

void	ft_algoritm(t_a *s, char **str)
{
	int i;
	int j;

	j = 0;
	while (str[j])
	{
		i = 0;
		while (str[j][i])
		{
			s->flat[ft_atoi(&str[j][i])].ant = 0;
			while (str[j][i] != '-' && str[j][i])
				i++;
			if (!str[j][i])
				break ;
			i++;
		}
		j++;
	}
	ft_run_ant(s, str);
}
